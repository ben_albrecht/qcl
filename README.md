qcl is a python package for managing the workflow and automating tasks that many computational chemists face on a daily basis. One could use the package as a set of scripts from the command line, but also could design a more specific workflow package based on qcl.

Here are some features currently available:

    figs                Quick plots of convergence criteria given an outputfile
    conformers          Naive conformer generation in Python
    obconformers        Open Babel conformer generation wrapper
    stretch             Stretch product at bond-forming atoms for transition state initial guess.
    templates           Template file management
    minima              Find minimum energy among many outputfiles
    zmatrix             Generate Z-Matrix from xyzfile



